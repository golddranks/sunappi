

use term_fb::{SimLoop, Buf, get_default_term, xy, xy_coord, InputKey};

fn main() {
    let (term_in, term_out) = get_default_term()
        .expect("Get term");

    let mut coord_oni_pole = xy_coord(20, 20);
    let mut coord_oni = xy_coord(0, 0);
    let mut oni_angle = 0.0;
    let mut coord_me = xy_coord(10, 10);
    let mut coord_dog = xy_coord(13, 16);
    let mut dir = xy(1, 0);

    let bottom_right = xy_coord(50, 50);

    let mut frame_bg = Box::new(Buf::new(bottom_right.x, bottom_right.y, '.'));
    frame_bg.show_fps = true;
    let mut sim_loop = SimLoop::new(30.0, 30.0, term_in, term_out)
        .expect("Construct SimLoop");

    let mut frame = frame_bg.clone();
    let mut input = InputState::new();
    let mut bullets = Vec::new();

    let mut del_list = vec![];

    while !input.quit {

        if input.down {
            coord_me.y += 1;
            dir = xy(0, 1);
        }
        if input.up {
            coord_me.y -= 1;
            dir = xy(0, -1);
        }
        if input.left {
            coord_me.x -= 1;
            dir = xy(-1, 0);
        }
        if input.right {
            coord_me.x += 1;
            dir = xy(1, 0);
        }

        if input.down_c {
            coord_dog.y += 1;
        }
        if input.up_c {
            coord_dog.y -= 1;
        }
        if input.left_c {
            coord_dog.x -= 1;
        }
        if input.right_c {
            coord_dog.x += 1;
        }

        if input.shoot {
            bullets.push((coord_me, dir));
        }

        oni_angle += 0.06;

        coord_oni.x = (coord_oni_pole.x as f64 + 15.0 * f64::cos(oni_angle)) as usize;
        coord_oni.y = (coord_oni_pole.y as f64 + 15.0 * f64::sin(oni_angle)) as usize;

        for (idx, (bullet_pos, bullet_dir)) in bullets.iter_mut().enumerate() {
            *bullet_pos += *bullet_dir;
            eprintln!("bullet_pos {:?}", bullet_pos);
            if !bullet_pos.inside_square_inc(xy_coord(1,1), bottom_right - xy(1, 1)) {
                del_list.push(idx);
            }
        }

        frame.copy_from(&frame_bg);
        frame.blit_line('o', coord_dog, coord_me);
        frame.blit_line('#', coord_oni_pole, coord_oni);
        frame.blit('鬼', coord_oni);
        frame.blit('俺', coord_me);
        frame.blit('犬', coord_dog);

        for &(bullet_pos, bullet_dir) in &bullets {
            if bullet_dir == xy(1, 0) || bullet_dir == xy(-1, 0) {
                frame.blit('-', bullet_pos);
            } else {
                frame.blit('|', bullet_pos);
            }
        }

        for idx in &del_list {
            bullets.remove(*idx);
        }
        del_list.clear();

        let (input_iter, used_frame) = sim_loop.ready(frame).expect("Ready");
        input = InputState::from_iter(input_iter);
        frame = used_frame;
    }
    frame.copy_from(&frame_bg);
    sim_loop.cleanup(frame).expect("Cleanup");
}


#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct InputState {
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,
    pub up_c: bool,
    pub down_c: bool,
    pub left_c: bool,
    pub right_c: bool,
    pub quit: bool,
    pub shoot: bool,
}

impl InputState {
    pub fn new() -> Self {
        Self {
            up: false,
            down: false,
            left: false,
            right: false,
            up_c: false,
            down_c: false,
            left_c: false,
            right_c: false,
            quit: false,
            shoot: false,
        }
    }

    pub fn from_iter<'a>(iter: impl Iterator<Item=InputKey<'a>>) -> Self {
        use term_fb::InputKey::*;

        let mut is = InputState::new();

        for item_key in iter {
            match item_key {
                Up => is.up = true,
                Down => is.down = true,
                Right => is.right = true,
                Left => is.left = true,
                AsciiPrintable(',') => is.up_c = true,
                AsciiPrintable('o') => is.down_c = true,
                AsciiPrintable('e') => is.right_c = true,
                AsciiPrintable('a') => is.left_c = true,
                AsciiPrintable('q') => is.quit = true,
                AsciiPrintable(' ') => is.shoot = true,
                _ => (),
            }
        }

        is
    }
}
